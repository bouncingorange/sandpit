Sandpit Wordpress Template
==========================

Boilerplate framework for rapid wordpress development using ACF and SASS

Installation steps
==================

1. Dependencies
2. Cloning
3. Database
4. Wordpress Installation
5. Theme Activation
6. Sass

1) Dependencies
---------------

(Once off) Install the following dependencies:

1. [RVM](https://rvm.io) and Ruby 1.9.3+
2. [Sass Gem](http://sass-lang.com/install)

2) Cloning
----------

#### A) Create your project directory

	
	mkdir sample.com.dev
	cd sample.com.dev
	
#### B) Configure apache


#### C) Clone the repository

Please not that the `.` is important.

	git clone git@bitbucket.org:bouncingorange/sandpit.git .
	
3) Database
-----------

### Set up the wordpress database

	mysqladmin create sample_com -u root -p
	mysql -Bne "GRANT ALL ON sample_com.* TO 'sample_com'@'localhost' IDENTIFIED BY 'PASSWORD';" -u root -p
	
4) Wordpress Installation
-------------------------

#### A) Edit the `wp-config.php` in the root directory

	nano wp-config.php

#### B) Find the section at ~ line 17 and replace database information from step 3
	
	// ** MySQL settings - You can get this info from your web host ** //
	/** The name of the database for WordPress */
	define('DB_NAME', 'sample_com');
	
	/** MySQL database username */
	define('DB_USER', 'sample_com');
	
	/** MySQL database password */
	define('DB_PASSWORD', 'PASSWORD');
	
#### C) Browse to `http://sample.com.dev` and follow the setup wizard.

5) Theme Activation
-------------------

#### A) Log in to the Wordpress dashboard when prompted or visit `http://sample.com.dev/wp-admin` 

Using the the details created in step 4C.

#### B) In Plugins / Installed plugins, activate Advanced Custom Fields and all related Advanced Custom Fields plugins (first 5 in list)

You can also activate all the other plugins you may need during development at this point.

#### C) In Appearance / Themes, activate the Sandpit theme

#### D) Browse to `http://sample.com.dev`

Notice the stylesheets are not loaded, this is due to the permalink structure and rewrite rules needing to be flushed in the next step.

#### E) Browse to Settings / Permalinks and just press save twice

Check the site again, notice that the stylesheet is loaded and the page displays correctly.

6) Sass
-------

#### A) Via SSH or terminal change to the `public_html/wp-content/themes/sandpit/` directory and activate Sass watch to run in the background

	cd public_html/wp-content/themes/sandpit/
	nohup sass --watch scss/config.scss:style.css &
	
Now any changes to the `.scss` files will result in a rebuild of style.css