# Edit the following settings

set :application, 'sandpit.dev'
set :repo_url, 'git@bitbucket.org:bouncingorange/sandpit.git'

# Stop editing here

set :scm, :git
set :deploy_via, :remote_cache

set :ssh_options, {
	# verbose: :debug
}

set :pty, false

set :format, :pretty
set :log_level, :debug

# set :linked_files, %w{htdocs/wp-config.php}
# set :linked_dirs, %w{htdocs/assets}

set :keep_releases, 3

namespace :deploy do
	task :restart do ; end
end