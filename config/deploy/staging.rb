set :stage, :staging

user = "ryan"

serveraddress = "staging.bouncingorange.com"

role :web, %W{#{user}@#{serveraddress}}

set :deploy_to, "/var/www/staging/sandpit.dev"

set :use_sudo, false

# Tasks
# =====

# Setup tasks

namespace :setup do

	desc "Add Assets directory"
  task :add_assets_dir do
  
  	on roles(:all) do
  		execute "if [ ! -d #{shared_path}/htdocs/assets ]; then mkdir #{shared_path}/htdocs/assets/	; fi"
  	end
  	
  end
  
end


# Deploy tasks

namespace :deploy do

	desc "Tell Wordpress what environment this is"
	task :add_env do
	
		on roles(:all) do
			execute "touch #{shared_path}/htdocs/env_staging"
		end
		
	end
    
  desc "Add Symlink to Assets directory"
  task :create_symlink do
  
	  on roles(:all) do
	  	execute "ln -nfs #{shared_path}/htdocs/assets #{release_path}/htdocs/assets"
	  end
	  
	end
	
	desc "Change owner and group"
  task :change_owner do
  
	  on roles(:all) do
	  	execute "sudo chown -R www-data #{release_path}/htdocs"
	  end
	  
	end
	
end

# Queue deploy tasks

before "deploy:started", "setup:add_assets_dir"
after "deploy:finishing", "deploy:create_symlink"
after "deploy:finishing", "deploy:add_env"


# Push tasks

namespace :push do

	desc "Upload wp-content directory"
	task :content do

    on roles(:all) do
	  	execute "rm -rf #{release_path}/htdocs/wp-content/*"
	  end
    
		run_locally do
			system "rsync --recursive --times --omit-dir-times --chmod=ugo=rwX --rsh=ssh --compress --human-readable --progress htdocs/wp-content/ #{user}@#{serveraddress}:#{release_path}/htdocs/wp-content/"
		end
		
	end

	desc "Upload themes directory"
	task :theme do
		
		on roles(:all) do
	  	execute "rm -rf #{release_path}/htdocs/wp-content/themes/*"
	  end
		
		run_locally do
			system "rsync --recursive --times --omit-dir-times --chmod=ugo=rwX --rsh=ssh --compress --human-readable --progress htdocs/wp-content/themes/ #{user}@#{serveraddress}:#{release_path}/htdocs/wp-content/themes/"
		end
		
	end
	
end


# Pull tasks

namespace :pull do
	
	desc "Download wp-content directory"
	task :content do
		
		run_locally do
			system "rm -rf htdocs/wp-content/*;
							rsync --recursive --times --omit-dir-times --chmod=ugo=rwX --rsh=ssh --compress --human-readable --progress #{user}@#{serveraddress}:#{release_path}/htdocs/wp-content/ htdocs/wp-content/"
		end
		
	end
	
	desc "Download themes directory"
	task :theme do
		
		run_locally do
			system "rm -rf htdocs/wp-content/themes/;
							rsync --recursive --times --omit-dir-times --chmod=ugo=rwX --rsh=ssh --compress --human-readable --progress #{user}@#{serveraddress}:#{release_path}/htdocs/wp-content/themes/ htdocs/wp-content/themes/"
		end
		
	end
	
end


# Sync tasks

namespace :sync do

  desc "Sync Assets directory"
  task :assets do
  
  	run_locally do
  		 system "rsync -avhur htdocs/assets/ -e 'ssh -p 22' #{user}@#{serveraddress}:#{shared_path}/htdocs/assets;
  		 				 rsync -avhur -e 'ssh -p 22' #{user}@#{serveraddress}:#{shared_path}/htdocs/assets/ htdocs/assets;"
  	end
  	
  end

end