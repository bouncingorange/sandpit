set :stage, :production

role :web, %w{ryan@sandpit.dev}

set :deploy_to, "/var/www/"