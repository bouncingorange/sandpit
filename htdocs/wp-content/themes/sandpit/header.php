<!doctype html>
<html lang="en" class="no-js">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="author" href="<?php bloginfo('url'); ?>/humans.txt" />
<meta name="generator" content="WordPress" />

<!--[if IE]><![endif]-->

<title><?php wp_title( '|', true, 'right' ); ?> <?php bloginfo('name'); ?></title>

<!-- Mobile specific -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimal-ui">

<!-- Stylesheets and Icons -->
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/apple-touch-icon.png">

<!-- Compiled SCSS -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">


<!-- Start Wordpress Head -->
<?php wp_head(); ?>

<!-- Backup jQuery -->
<script>window.jQuery || document.write('<script src="<?php echo bloginfo('url'); ?>/wp-includes/js/jquery/jquery.js"><\/script>')</script>

<!--[if (gte IE 6)&(lte IE 8)]>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/libs/rem.min.js"></script>
<![endif]-->

<!-- End Wordpress Head -->

</head><?php $body_classes = join( ' ', get_body_class() ); ?>
<!--[if lt IE 7 ]><body class="ie6 <?php echo $body_classes; ?>"><![endif]-->
<!--[if IE 7 ]><body class="ie7 <?php echo $body_classes; ?>"><![endif]-->
<!--[if IE 8 ]><body class="ie8 <?php echo $body_classes; ?>"><![endif]-->
<!--[if IE 9 ]><body class="ie9 <?php echo $body_classes; ?>"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body class="<?php echo $body_classes; ?>"><!--<![endif]-->

<div id="page-wrapper">

	<div class="wrapper" id="header-wrapper">

		<header class="section" id="header" role="banner">

			<div class="section-content" id="header-content">
	
				<div id="logo">
	
					<a href="<?php bloginfo('url'); ?>/">
	
						<img src="<?php bloginfo('template_url'); ?>/logo.png" alt="<?php wp_title(); ?> Logo" title="<?php wp_title(); ?>"/>
	
						<h1><?php echo bloginfo('title'); ?></h1>
	
					</a>
	
				</div>
			
			</div>
			
		</header>

	</div>

	<div class="wrapper" id="nav-wrapper">

		<div class="menu-header">

			<nav id="main-nav" role="navigation">

				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary', 'container' => 'false' ) ); ?>

			</nav>

		</div>

	</div>

	<?php if (is_front_page()): ?>

		<?php get_template_part('banner'); ?>
	
	<?php endif; ?>
