	
	<div class="wrapper" id="footer-wrapper">
	
		<footer class="section" id="footer" role="contentinfo">
		
			<div class="section-content" id="footer-content">
			
				<nav class="footer-nav">
			
					<ul>
					
						<li>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></li>
											
					</ul>
				
				</nav>
			
			</div>
			
		</footer>
	
	</div><!-- End #footer-wrapper -->

</div><!-- End #page-wrapper -->

<!-- Start Wordpress Footer -->

<?php wp_footer(); ?>

<!-- End Wordpress Footer -->

</body>
</html>