<div class="content-blocks">

							<?php if (get_the_content()): ?>
							<div class="standard">

								<?php if (is_singular() && !is_front_page()) { the_content(''); } else { the_excerpt();} ?>

							</div>
							<?php endif;

							$numtabs = 0;
							$numwidths = 0;

							while (the_flexible_field('content')): ?>

								<?php /*// Text Block ///////*/ if (get_row_layout() == 'heading'): ?>

									<h<?php the_sub_field('heading_level'); ?> class="heading <?php if (get_sub_field('padding')) { echo 'vertical-padding'; } ?>">

										<?php the_sub_field('content'); ?>

									</h<?php the_sub_field('heading_level'); ?>>

								<?php /*// Text Block ///////*/ elseif (get_row_layout() == 'text_block'): ?>

									<div class="standard <?php if (get_sub_field('padding')) { echo 'vertical-padding'; } ?>">

										<?php the_sub_field('content'); ?>

									</div>

								<?php /*// Full Width Text Block ///////*/ elseif (get_row_layout() == 'full_width'): $numwidths++; ?>

									<div class="full-width-wrapper" id="<?php echo 'full-width-' . $numwidths; ?>">

										<style>
											<?php echo '#full-width-' . $numwidths; ?> h1,
											<?php echo '#full-width-' . $numwidths; ?> h2,
											<?php echo '#full-width-' . $numwidths; ?> h3,
											<?php echo '#full-width-' . $numwidths; ?> h4,
											<?php echo '#full-width-' . $numwidths; ?> h5,
											<?php echo '#full-width-' . $numwidths; ?> h6,
											<?php echo '#full-width-' . $numwidths; ?> p {
												color: <?php the_sub_field('text_colour'); ?>;
												<?php if (get_sub_field('text_shadow')) { echo 'text-shadow: 0 1px 0 ' . get_shadow_colour(get_sub_field('text_colour')) . ';'; } else { echo 'text-shadow: none;'; } ?>
											}

										</style>

										<div class="full-width <?php if (get_sub_field('padding')) { echo 'vertical-padding'; } ?>" style="background-color: <?php the_sub_field('background_colour'); ?>; <?php $image = get_sub_field('background_image'); if ($image) { echo "background-image: url(" . $image['url'] . ")"; } ?>">

											<div class="center">

												<div class="standard"><?php the_sub_field('content'); ?></div>

											</div>

										</div>

									</div>

								<?php /*// Columns //////*/ elseif (get_row_layout() == 'columns'): ?>

								<?php if (get_sub_field('columns')): ?>

								<?php $columns = get_sub_field('columns'); ?>

								<?php $count = count($columns); ?>

								<div class="columns columns-<?php echo $count; ?> <?php if (get_sub_field('padding')) { echo 'vertical-padding'; } ?>">

									<?php foreach ($columns as $column): ?>

										<div class="single-column" >

											<?php echo $column['content']; ?>

										</div>

									<?php endforeach; ?>

								</div>

								<?php endif; ?>

								<?php /*// Odd Columns //////*/ elseif (get_row_layout() == 'odd_columns'): ?>

								<?php if (get_sub_field('columns')): ?>

								<?php $columns = get_sub_field('columns'); ?>

								<?php $count = count($columns); ?>

								<div class="columns columns-<?php echo $count; ?> <?php if (get_sub_field('padding')) { echo 'vertical-padding'; } ?>">

									<?php foreach ($columns as $column): ?>

										<div class="odd-column <?php echo $column['size']; ?>" >

											<?php echo $column['content']; ?>

										</div>

									<?php endforeach; ?>

								</div>

								<?php endif; ?>

								<?php /*// Tabs /////////*/ elseif (get_row_layout() == 'tabs'):?>

									<?php if (get_sub_field('tabs')): ?>

									<?php $numtabs++; ?>

									<script type="text/javascript">
										 	$(document).ready(function() {

										 		// Horizontal Sliding Tabs demo
										 		$('div#tabs<?php echo $numtabs; ?>').slideTabs({
												// Options
												contentAnim: 'fade',
												contentAnimTime: 600,
												contentEasing: 'easeInOutExpo',
												autoHeight: true,
												autoHeightTime: '500'
										 		});

										 		$('.st_tab').click( function() {
										 			var hash = $(this).attr('rel');
										 			window.location.hash = hash;

										 		});

										 	});
										</script>

									<?php $tabs = get_sub_field('tabs'); ?>

									<?php $count = count($tabs); ?>

									<div id="tabs<?php echo $numtabs; ?>" class="tabs <?php if (get_sub_field('padding')) { echo 'vertical-padding'; } ?>">

												<div class="st_tabs_container">

														<div class="st_slide_container">

												<ul class="st_tabs">

												<?php $i = 1; ?>

												<?php foreach($tabs as $tab): ?>

													<li><a class="st_tab <?php if ($i == 1) { ?>st_first_tab st_tab_active<?php } ?>" rel="<?php echo $numtabs . $i; ?>" href="#tab<?php echo $numtabs . $i; ?>"><?php echo $tab['title']; ?></a></li>

													<?php $i++; ?>

												<?php endforeach; ?>

												</ul>

											</div> <!-- /.st_slide_container -->

												</div> <!-- /.st_tabs_container -->

										<div class="st_view_container">

											<div class="st_view">

											<?php $o = 1; ?>

											<?php foreach($tabs as $tab): ?>

												<div id="tab<?php echo $numtabs . $o; ?>" class="st_tab_view <?php if ($o == 1) { ?>st_first_tab_view<?php } ?>">

													<?php echo $tab['content']; ?>

												</div>

												<?php $o++; ?>

											<?php endforeach; ?>

											</div> <!-- /.st_view -->

												</div> <!-- /.st_view_container -->

										</div> <!-- /#st_horizontal -->

									<?php endif; ?>

								<?php /*// Slideshow ////*/ elseif (get_row_layout() == 'slideshow'):?>

									<figure class="slideshow <?php if (get_sub_field('padding')) { echo 'vertical-padding'; } ?>">

									<?php if(get_sub_field('slideshow')): ?>

										<script>
											$(function() {

												$('.flexslider').flexslider({
													directionNav: false,
													slideshowSpeed: 4000
												});

											});
										</script>

										<div class="flexslider">

											<ul class="slides">

											<?php $imagearray = get_sub_field('slideshow'); ?>

											<?php foreach($imagearray as $image): ?>

												<li>

													<img src="<?php echo $image['sizes']['large']; ?>" alt="" />

												</li>

											<?php endforeach; ?>

									 		</ul>

										</div>

									<?php endif; ?>

								</figure>

							<?php /*// Gallery ////*/ elseif (get_row_layout() == 'gallery'):?>

								<?php

								$columns = get_sub_field('columns');

								$numbers = array("zero", "one", "two", "three", "four", "five", "six");

								$sizes = array("full", "large", "large", "medium", "medium", "medium" )

								?>

								<div class="gallery <?php if (get_sub_field('padding')) { echo 'vertical-padding'; } ?>">

									<div class="row <?php echo $columnclass = $numbers[$columns]; ?>">

										<?php $i = 1; $images = get_sub_field('gallery'); if ($images) { foreach( $images as $image ): ?>

										<a rel="fancybox" href="<?php echo $image['url']; ?>"><figure>

											<img title="<?php echo $image['title']; ?>" src="<?php echo $image['sizes'][$sizes[$columns]]; ?>" alt="<?php echo $image['alt']; ?>" />

											<?php if (get_sub_field('captions') && $image['caption']) { ?><figcaption><?php echo $image['caption']; ?></figcaption><?php } ?>

										</figure></a>

									<?php if ( $i % $columns == 0 ) { ?>

									</div>

									<div class="row <?php echo $columnclass; ?>">

									<?php } $i++; ?>

										<?php endforeach; } ?>

									</div>

								</div>

								<?php /*// Accordion ////*/ elseif (get_row_layout() == 'accordion'):?>

									<div class="accordion">

										<div class="accordion-title"><?php the_sub_field('title'); ?></div>

										<div class="accordion-content">

											<?php the_sub_field('content'); ?>

										</div>

									</div>

								<?php endif; ?>

							<?php endwhile; ?>

						</div>
