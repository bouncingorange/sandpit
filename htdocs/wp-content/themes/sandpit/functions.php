<?php

include_once("includes/theme-utilities.php");

$theme_name = explode('/themes/', get_template_directory());

define('WP_BASE', wp_base_dir());
define('THEME_NAME', next($theme_name));
define('RELATIVE_PLUGIN_PATH', str_replace(site_url() . '/', '', plugins_url()));
define('FULL_RELATIVE_PLUGIN_PATH', WP_BASE . '/' . RELATIVE_PLUGIN_PATH);
define('RELATIVE_CONTENT_PATH', str_replace(site_url() . '/', '', content_url()));
define('THEME_PATH', RELATIVE_CONTENT_PATH . '/themes/' . THEME_NAME);

include_once("includes/theme-activation.php");
include_once("includes/theme-setup.php");
include_once("includes/theme-enqueue.php");
include_once("includes/theme-content.php");
include_once("includes/theme-widgets.php");
include_once("includes/theme-options.php");

