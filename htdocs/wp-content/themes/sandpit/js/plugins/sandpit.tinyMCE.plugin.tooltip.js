function tooltipplugin() {
    return '[tooltip tip="" text=""]';
}

(function() {

    tinymce.create('tinymce.plugins.tooltipplugin', {

        init : function(ed, url){
            ed.addButton('tooltipplugin', {
                title : 'Insert tooltip',
                onclick : function() {
                    ed.execCommand(
                        'mceInsertContent',
                        false,
                        tooltipplugin()
                        );
                },
                image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAaCAQAAAAOXfQCAAAAXUlEQVQ4y2P4z0AeZBjVSKTG+/8n/y/HASf/v/cfp0bc2iBacWoESWMHEK14NeKDtNH4H49jB71TidBIdjySnXLQIUzT7v8kJnLc2ojQiF0bQY24tBHQiFvbyChzADs8QvHvyFtRAAAAAElFTkSuQmCC"
            });
        },
    });

    tinymce.PluginManager.add('tooltipplugin', tinymce.plugins.tooltipplugin);
    
})();
