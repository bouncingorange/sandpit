/* Variables
========================================================================== */

var heroSly;

/* Breakpoints
========================================================================== */

enquire.register("screen and (max-width: 480px)", {

	match : function() {
		
	},
	
	unmatch : function() {
		
	}
	
});


/* General
========================================================================== */

$(function() {
	
	/* Initialise
	========================================================================== */
	
	FastClick.attach(document.body);
	
	InstantClick.init();
	
	$.scoped();
		
	$.adaptiveBackground.run({
		selector:	'[data-adaptive-background="1"]',
		parent:		null,
	});
	
	/* if (!window.scrollReveal) window.scrollReveal = new scrollReveal(); */
	
	/* Global
	========================================================================== */
	
	$('a[href="#"]').addClass('fauxlink').click(function(event) {
		event.preventDefault();
	});
	
	
	/* Hero Slider
	========================================================================== */
	
	if ($('#hero').length > 0) {
	
		heroSly = new Sly('#hero', {
			horizontal: 1,
			itemNav: "forceCentered",
			itemSelector: "#hero .slides li",
			smart: 1,
			activateMiddle: 1,
			activateOn: "click",
			cycleBy: "items",
			cycleInterval: 3500,
			prev: "#hero .prev",
			next: "#hero .next",
			pauseOnHover: 1,
			mouseDragging: 1,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 0,
			speed: 1200,
			elasticBounds: 1,
			easing: "easeOutExpo",
			keyboardNavBy: "items"
		}).init();
		
		refreshSly = function() {
			
			heroSly.reload();
			
			$('.frame .slides li').each(function() {
				$(this).css('width', heroSly.rel.frameSize + 20);	
			});
			
			heroSly.reload();
			
		}
		
		refreshSly();
		
		$(window).resize(function() {
			waitForFinalEvent(function() {
				refreshSly();
			}, 500, "HeroSlider");
			
		});
	
	}
	
	
	/* Menu effects
	========================================================================== */

	var menuToggle = '<div id="menu-toggle">☰ Menu</div>',
			siteOverlay = '<div class="site-overlay"></div>';

	$('#header-wrapper').prepend(menuToggle);
	$('body').prepend(siteOverlay);
	
	
	// $('#nav-wrapper').prepend(menuToggle);
	
			
	$('#menu-toggle').click(function() {
	
		$('html').toggleClass('js-nav');
		$('#nav-wrapper').focus();
		
	});
	
	$('#main-nav li').hoverIntent( {
	
	
	over: function() {
		
		$(this).children('.sub-menu').show(0, function() {
			$(this).css("overflow","visible");
		});
		if ($(this).children('.sub-menu').length) {
			$(this).addClass('active');
		}
	}, timeout: 200,
	
	
	out: function() {
		
		$(this).children('.sub-menu').delay(200).hide(0, function() {
			$(this).parent().removeClass('active');
		}).css("overflow","visible");
		
		
	} });
	
	
	$('#main-nav li .sub-menu').parent('li').addClass('menu-parent');
	
	$('.current-menu-item').closest('.menu-parent').addClass('current-menu-parent');
	
	$('.site-overlay').click(function(event) {
		
		if ($('html').hasClass('js-nav')) {
			event.stopPropagation();
		 	$('html').removeClass('js-nav');
		 	event.preventDefault();
		}
		
	});
	
	
	/* Accordion
	========================================================================== */

	$('.accordion').click(function() {
		$(this).children('.accordion-content').slideToggle(300, 'easeInOutExpo');
		$(this).children('.accordion-title').toggleClass('active');
	});
	
	
	/* Quotes
	========================================================================== */
	
	$( '#sandpit-quotes' ).sandpitQuotes();


	/* Stop auto scrolling on user override
	========================================================================== */

	if(window.addEventListener) document.addEventListener('DOMMouseScroll', stopScroll, false);
		document.onmousewheel = stopScroll;
	
	function stopScroll() {
		$('html, body').stop(true, false);	// Stops and dequeue's animations
	}
	
	
	/* ScrollTo
	========================================================================== */

	function scrollTo(anchor) {
			
		$('html, body').stop().animate({
		
			scrollTop: $(anchor).offset().top
			
		}, 1200,'easeInOutExpo');
	
	}


	/* Infinite scroll
	========================================================================== */
	
	if ($('body').not('.paged').hasClass('archive')) {
		
		var ias = $.ias({
			container:	'#content',
			item:				'.post',
			pagination: '.paging',
			next:				'.paging a.next',
			delay:			'1000',
			
		});
				
		ias.extension(new IASTriggerExtension({
			html:	'<div class="ias-trigger" style="text-align: center; cursor: pointer;"><a data-no-instant>{text}</a></div>'
		}));
		ias.extension(new IASSpinnerExtension());
		ias.extension(new IASNoneLeftExtension());
		ias.extension(new IASPagingExtension());
	
		
		ias.on('pageChange', function(pageNum, scrollOffset, url) {
			history.pushState(null, null, url);
		});
		
	}
	
});


/* Helpers
========================================================================== */

var hasParent = function(el, id)
{
		if (el) {
				do {
						if (el.id === id) {
								return true;
						}
						if (el.nodeType === 9) {
								break;
						}
				}
				while((el = el.parentNode));
		}
		return false;
};

var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) {
			uniqueId = "Don't call this twice without a uniqueId";
		}
		if (timers[uniqueId]) {
			clearTimeout (timers[uniqueId]);
		}
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();
