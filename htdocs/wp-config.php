<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

if ( file_exists( dirname( __FILE__ ) . '/env_local' ) ) {

    // Local Environment
    define('WP_ENV', 'local');
    define('WP_DEBUG', true);

    define('DB_NAME', 'sandpit_dev');
    define('DB_USER', 'sandpit_dev');
    define('DB_PASSWORD', 'sandpit_dev');
    define('DB_HOST', 'mysql.bouncingorange.com');
		
		define('WP_HOME','http://sandpit.dev');
		define('WP_SITEURL','http://sandpit.dev');

} elseif ( file_exists( dirname( __FILE__ ) . '/env_staging' ) ) {

    // Staging Environment
    define('WP_ENV', 'staging');
    define('WP_DEBUG', false);

    define('DB_NAME', 'sandpit_dev');
    define('DB_USER', 'sandpit_dev');
    define('DB_PASSWORD', 'sandpit_dev');
    define('DB_HOST', 'staging.bouncingorange.com');
    
    define('WP_HOME','http://example.staging.bouncingorange.com');
		define('WP_SITEURL','http://example.staging.bouncingorange.com');
    
} else {

    // Production Environment
    define('WP_ENV', 'production');
    define('WP_DEBUG', false);

    define('DB_NAME', 'sandpit_dev');
    define('DB_USER', 'sandpit_dev');
    define('DB_PASSWORD', 'sandpit_dev');
    define('DB_HOST', 'staging.bouncingorange.com');
    
    define('WP_HOME','http://sandpit.com');
		define('WP_SITEURL','http://sandpit.com');
    
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F3+pr5IkPicNC{9)^Q+Af4O*67xSn(OBa3,+&+Vlk09XEV%uaBeKOQ{W+;d85f<D');
define('SECURE_AUTH_KEY',  '#.+-tv2Y+D Y0=9g5TV#+yb=@ugEdvqS#12Dc|U|]y-TE0!P8v|,t$q/zJh@XM5h');
define('LOGGED_IN_KEY',    'XG2N0HsV>#Pj6jkb&*ZahFiSI6f/6[$j6u*zAT?K?OekhbqBz)kno8+N7= ]5e&:');
define('NONCE_KEY',        ',0u<pVM+N6?<II?QHOf--)e*79g3FKO,ps@Fc=nY[~FU]AU+@&$LRp.;Z[Ze,IxN');
define('AUTH_SALT',        'r6W9r6I|z|4xomKI[5GsC_|k+|X9w5h+z=Qwz%43[ZUF7 48-jF@{I<YAzr>G7u?');
define('SECURE_AUTH_SALT', '$8QQj|d{l24K5H2X]E);R-)9-A$-zP.Ib,C>Dj8[b@cd5?ah^vR$|<h0/WoObFUA');
define('LOGGED_IN_SALT',   '.EE4*MuW|=B%1U,^dwlDZ,2z%XM6JlRblvp6/liB{dz<v-6?v|NU}LF&me?}tRD]');
define('NONCE_SALT',       'aRM3/@AUb6)V*&5fIj0n@JH]2#d41c(b!+Km2!yMHXln4PjL5r#V=a,NS[ujR5VI');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');